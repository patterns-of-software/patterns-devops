# Docker Compose - GitLab Container Registry

Esta guía proporciona instrucciones sobre cómo ejecutar el archivo de Docker Compose `docker-compose.yml` que obtiene imágenes del Container Registry. Esta opción permite ejecutar la aplicación en la última version sin necesidad de clonar los repositorios individuales.

## Requisitos previos

Antes de continuar, asegurarse de tener instalados los siguientes requisitos previos:

1. Docker: Asegurarse de tener Docker instalado en tu sistema. Puedes descargar Docker desde el sitio web oficial: [https://www.docker.com/get-started](https://www.docker.com/get-started).

2. Docker Compose: Instala Docker Compose para gestionar aplicaciones con múltiples contenedores. Puedes encontrar instrucciones de instalación para tu sistema operativo específico aquí: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/).

## Paso 1: Iniciar sesión en el Registro de Contenedores de GitLab

Para obtener imágenes del Registro de Contenedores de GitLab, se necesita iniciar sesión con tus credenciales de GitLab. Pasos de cómo hacer esto:

1. Abre una terminal o símbolo del sistema.

2. Inicia sesión en el Container Registry de GitLab utilizando la CLI de Docker ejecutando el siguiente comando:
```bash
docker login registry.gitlab.com
```

3. Ingresa tu nombre de usuario y contraseña de GitLab cuando se te solicite. Asegurate de tener suficientes privilegios para acceder al registro.

4. Una vez autenticado, vas a recibir un mensaje de éxito que indica una sesión iniciada correctamente.

Para obtener más detalles sobre cómo iniciar sesión en el Registro de Contenedores de GitLab, consultar la documentación oficial de GitLab: [https://docs.gitlab.com/ee/user/packages/container_registry/](https://docs.gitlab.com/ee/user/packages/container_registry/).

## Paso 2: Ejecutar el archivo Docker Compose

1. Abre una terminal o símbolo del sistema.

2. Navega hasta el directorio que contiene el archivo `docker-compose.yml`.

3. Ejecuta el siguiente comando para iniciar los servicios definidos en el archivo `docker-compose.yml`:
```bash
docker-compose up
```

Docker Compose obtendrá la imagen requerida del Container Registry de GitLab y comenzará los servicios especificados.

Para obtener más detalles sobre el uso de Docker Compose, consulta la documentación oficial de Docker: [https://docs.docker.com/compose/](https://docs.docker.com/compose/).

¡Eso es todo! Has ejecutado correctamente el archivo de Docker Compose y deberías tener todos los servicios corriendo.
