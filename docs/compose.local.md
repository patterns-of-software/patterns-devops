# Docker Compose - Repositorios locales

Esta guía proporciona instrucciones sobre cómo ejecutar el archivo de Docker Compose `docker-compose.local.yml` que obtiene imágenes de los repositorios clonados localmente. Esta opción permite ejecutar la aplicación utilizando los repositorios que se encuentran en la máquina. Recomendado para desarrolo.

## Requisitos previos

Antes de continuar, asegurarse de tener instalados los siguientes requisitos previos:

1. Docker: Asegurarse de tener Docker instalado en tu sistema. Puedes descargar Docker desde el sitio web oficial: [https://www.docker.com/get-started](https://www.docker.com/get-started).

2. Docker Compose: Instala Docker Compose para gestionar aplicaciones con múltiples contenedores. Puedes encontrar instrucciones de instalación para tu sistema operativo específico aquí: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/).

## Paso 1: Clonar los repositorios

Para poder iniciar la aplicacion se necesita clonar los diferentes repositorios y ubicarlos al mismo nivel del filesystem

1. Abre una terminal o símbolo del sistema.

2. Crear un directorio para contener a los diferentes repositorios (Es importante que se encuentren todos los repositorios al mismo nivel del filesystem) 

3. Clona los repositorios del proyecto
```bash
git clone git@gitlab.com:patterns-of-software/patterns-delivery.git

git clone git@gitlab.com:patterns-of-software/patterns-idp.git

git clone git@gitlab.com:patterns-of-software/patterns-products.git

git clone git@gitlab.com:patterns-of-software/patterns-shopping.git
```

## Paso 2: Ejecutar el archivo Docker Compose

1. Abre una terminal o símbolo del sistema.

2. Navega hasta el directorio que contiene el archivo `docker-compose.local.yml`.

3. Ejecuta el siguiente comando para iniciar los servicios definidos en el archivo `docker-compose.local.yml`:
```bash
docker-compose -f docker-compose.local.yml up
```

Docker Compose obtendrá las imagenes de los diferentes directorios y comenzará los servicios especificados.

Para obtener más detalles sobre el uso de Docker Compose, consulta la documentación oficial de Docker: [https://docs.docker.com/compose/](https://docs.docker.com/compose/).

¡Eso es todo! Has ejecutado correctamente el archivo de Docker Compose y deberías tener todos los servicios corriendo.

