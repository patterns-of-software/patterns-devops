# patterns-devops
Este repositorio proporciona documentación y herramientas para el despliegue y desarrollo del sistema.

## Docker Compose - GitLab Container Registry
La documentación completa para poder ejecutar los diferentes servicios utilizando las imagenes del Container registry se encuentra en el archivo [compose.md](./docs/compose.md). Asegúrate de revisar ese archivo para obtener información detallada sobre cómo ejecutar la aplicación.

## Docker Compose - Repositorios locales
La documentación completa para levantar los diferentes servicios utilizando las imagenes locales se encuentra en el archivo [compose.local.md](./docs/compose.local.md). Asegúrate de revisar ese archivo para obtener información detallada sobre cómo ejecutar la aplicación.